

// Define the data for the chart
var data = {
    labels: ['Math', 'Science', 'English', 'History', 'Art'],
    datasets: [
        {
            label: 'Marks',
            backgroundColor: [],
            borderColor: 'rgb(75, 192, 192)',
            borderWidth: 1,
            data: [70, 80, 20, 75, 85],
        }
    ]
};

// Define the colors for the bars
var colors = [];
for (var i = 0; i < data.datasets[0].data.length; i++) {
    var mark = data.datasets[0].data[i];
    if (mark >= 80) {
        colors.push('green');
    } else if (mark >= 40 && mark < 80) {
        colors.push('blue');
    } else {
        colors.push('red');
    }
}
data.datasets[0].backgroundColor = colors;

// Get the canvas element
var canvas = document.getElementById('myChart');

// Create the chart object
var chart = new Chart(canvas, {
    type: 'bar',
    data: data,
    options: {
        
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        
    }
});






        