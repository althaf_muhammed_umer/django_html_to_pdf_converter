from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('convert/', views.convert_to_pdf, name='convert_to_pdf'),
    path('generate_hall_ticket/', views.generate_html_to_pdf),


   
    path('chart', views.chart, name='chart'),
    

    
]
