from django.shortcuts import render

from django.http import HttpResponse
import asyncio
import time
import pyautogui
from PIL import Image

from xhtml2pdf import pisa

# Create your views here.

from django.template.loader import get_template

from selenium import webdriver
from pyppeteer import launch

from selenium.webdriver.chrome.options import Options
import io

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch
from django.template.loader import render_to_string

import os
import tempfile
import subprocess


def index(request):
    return render(request, 'index.html')

def convert_to_pdf(request):
    # Define the URL of the HTML template to be converted
    url = 'http://localhost:8000/'

    # Set up ChromeDriver options
    chrome_options = Options()
    chrome_options.add_argument('--headless')  # Run Chrome in headless mode (no GUI)
    chrome_options.add_argument('--disable-gpu')  # Disable GPU acceleration (useful for headless mode)

    # Initialize ChromeDriver
    driver = webdriver.Chrome(options=chrome_options)

    # Load the HTML template in ChromeDriver
    driver.get(url)

    # Get the dimensions of the HTML template
    width = driver.execute_script('return document.documentElement.scrollWidth')
    height = driver.execute_script('return document.documentElement.scrollHeight')

    # Set the window size of ChromeDriver to match the dimensions of the HTML template
    driver.set_window_size(width, height)

    # Take a screenshot of the HTML template using ChromeDriver
    screenshot = driver.get_screenshot_as_png()

    # Close ChromeDriver
    driver.quit()

    # Create a byte stream to store the image data
    image_stream = io.BytesIO(screenshot)

    # Create a temporary file to store the image data
    temp_file = tempfile.NamedTemporaryFile(delete=False, suffix='.png')

    # Write the image data to the temporary file
    temp_file.write(screenshot)

    # Close the temporary file
    temp_file.close()

    # Create a PDF file using reportlab
    pdf_buffer = io.BytesIO()
    c = canvas.Canvas(pdf_buffer, pagesize=letter)

    # Draw the image on the PDF canvas
    c.drawImage(temp_file.name, 0, 0, letter[0], letter[1])

    # Save the PDF file
    c.showPage()
    c.save()

    # Delete the temporary file
    os.unlink(temp_file.name)

    # Reset the byte stream position to the beginning
    pdf_buffer.seek(0)

    # Create an HTTP response with the PDF file
    response = HttpResponse(pdf_buffer.read(), content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="template.pdf"'

    return response


def generate_html_to_pdf(request):
    # html_string = render_to_string('exam_hall_ticket/st_anthony_school_hall_ticket.html', context)
    template = get_template('test.html')
    html = template.render()
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="hallticket.pdf"'
    pisa.CreatePDF(html, dest=response)
    return response



def chart(request):
    sales_data = [
        {'product': 'Product A', 'sales': 100},
        {'product': 'Product B', 'sales': 200},
        {'product': 'Product C', 'sales': 150},
        {'product': 'Product D', 'sales': 50},
    ]
    context = {
        'sales_data': sales_data,
    }
    return render(request, 'newtest.html', context)
    



# from django.conf import settings
# from django.template.loader import get_template
# from django.http import HttpResponse

# import imgkit
# import os


# # path to wkhtmltoimage.exe file
# wkhtml_to_image = os.path.join(
#     settings.BASE_DIR, "wkhtmltoimage.exe")


# def html_to_image(request):
#     template_path = 'progresscard.html'
#     template = get_template(template_path)

#     # Render the template to a string
#     html = template.render()

#     config = imgkit.config(wkhtmltoimage=wkhtml_to_image, xvfb='/opt/bin/xvfb-run')

#     image = imgkit.from_string(html, False, config=config)

#     # Generate download
#     response = HttpResponse(image, content_type='image/jpeg')

#     response['Content-Disposition'] = 'attachment; filename=image.jpg'

#     if response.status_code != 200:
#         return HttpResponse('We had some errors <pre>' + html + '</pre>')

#     return response













